/* globals beforeEach */
const { describe, it } = require('mocha')
const { expect } = require('chai')
const request = require('supertest')

describe('Graph API', () => {
  let server

  beforeEach(() => {
    process.env.NODE_ENV = 'TEST'
    server = require('./server')
  })

  it('returns the car of the week', done => {
    request(server)
      .post('/graphql')
      .send({query: `
        {
          carOfTheWeek {
            id
            makeId
            make
          }
        }
      `})
      .end((err, res) => {
        if (err) { return done(err) }
        const response = JSON.parse(res.text)
        expect(response).to.deep.equal({
          data: {
            carOfTheWeek: {
              id: 520,
              makeId: 50,
              make: 'Mazda'
            }
          }
        })
        done()
      })
  })

  it('returns valid makes list', done => {
    request(server)
      .post('/graphql')
      .send({query: `
        {
          makes {
            id
            name
          }
        }
      `})
      .end((err, res) => {
        if (err) { return done(err) }
        const response = JSON.parse(res.text)
        expect(response).to.have.property('data')
        expect(response.data).to.have.property('makes')
        expect(response.data.makes[0]).to.deep.equal({
          id: 10,
          name: 'Porsche'
        })
        done()
      })
  })

  it('returns models by makeId', done => {
    request(server)
      .post('/graphql')
      .send({query: `
        {
          models(makeId: 10) {
            id
            name
          }
        }
      `})
      .end((err, res) => {
        if (err) { return done(err) }
        const response = JSON.parse(res.text)
        expect(response).to.have.property('data')
        expect(response.data).to.have.property('models')
        expect(response.data.models[0]).to.deep.equal({
          id: 100,
          name: '911 Carrera 4s'
        })
        done()
      })
  })

  it('returns model by id', done => {
    request(server)
      .post('/graphql')
      .send({query: `
        {
          model(modelId: 520) {
            id
            name
            make
          }
        }
      `})
      .end((err, res) => {
        if (err) { return done(err) }
        const response = JSON.parse(res.text)
        expect(response).to.have.property('data')
        expect(response.data).to.have.property('model')
        expect(response.data.model).to.deep.equal({
          id: 520,
          name: 'MX-5',
          make: 'Mazda'
        })
        done()
      })
  })
})
