const express = require('express')
const graphqlHTTP = require('express-graphql')
const cors = require('cors')
const pino = require('express-pino-logger')()
const { schema, root } = require('./graphql')
const { NODE_ENV: env } = process.env
const app = express()

if (env !== 'TEST') {
  app.use(pino)
}

app.use('/graphql', cors(), graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}))

app.listen(3300)

module.exports = app
