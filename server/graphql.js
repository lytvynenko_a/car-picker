const { buildSchema } = require('graphql')
const cotw = require('../json/cotw.json')
const models = require('../json/models.json')
const makes = require('../json/makes.json')

const RESPONSE_DELAY = process.env.NODE_ENV === 'TEST' ? 0 : 500

var schema = buildSchema(`
  type Query {
    carOfTheWeek: CarOfTheWeek
    makes: [Make]
    models(makeId: Int!): [Model]
    model(modelId: Int!): Model
  }

  type CarOfTheWeek {
    id: Int!
    makeId: Int!
    name: String!
    price: Float!
    imageUrl: String!
    review: String!
    make: String!
  }

  type Make {
    id: Int!
    name: String!
  }

  type Model {
    id: Int!
    makeId: Int!
    make: String
    name: String!
    price: Float!
    imageUrl: String!
  }
`)

const root = {
  carOfTheWeek: () => {
    // just a emulation of async upstream request
    return new Promise((resolve, reject) => {
      const model = models.find((model) => model.id === cotw.modelId)
      const make = makes.find((make) => make.id === model.makeId)
      if (!model) return reject(new Error('Model not found'))
      setTimeout(() =>
        resolve(Object.assign(model, { review: cotw.review, make: make.name }))
        , RESPONSE_DELAY)
    })
  },
  makes: () => {
    return new Promise((resolve, reject) => {
      if (!makes) return reject(new Error('Makes not found'))
      setTimeout(() =>
        resolve(makes)
        , RESPONSE_DELAY)
    })
  },
  models: (query) => {
    return new Promise((resolve, reject) => {
      const result = models.filter((model) => parseInt(model.makeId) === query.makeId)
      if (!result) return reject(new Error('Models query error: not found'))
      setTimeout(() =>
        resolve(result)
      , RESPONSE_DELAY)
    })
  },
  model: (query) => {
    return new Promise((resolve, reject) => {
      const model = models.find((model) => model.id === query.modelId)
      if (!model) return reject(new Error('Model not found'))
      const make = makes.find((make) => make.id === model.makeId)
      if (!make) return reject(new Error('Model make not found'))
      setTimeout(() =>
        resolve(Object.assign(model, { make: make.name }))
        , RESPONSE_DELAY)
    })
  }
}

module.exports = {
  schema,
  root
}
