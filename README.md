# Qantas Car Picker - Tech Test

![Demo](http://i.imgur.com/7jCPRwu.gif "Demo")

## Installation

### Prerequisites
 - node 6, npm 3

### Clone repository
`git clone https://bitbucket.org/lytvynenko_a/car-picker.git`

Change folder:

`cd car-picker`

### Install dependencies
`npm install`

### Done! Now you can start the app
`npm start`

The front-end will work on `http://localhost:3333`, GraphiQL UI on `http://localhost:3300/graphql`

## Stack

### Front-end
 - ReactJS
 - React-Router
 - Apollo-Client (GraphQL client)
 - Material-UI
 - Jest
 - Enzyme
### Back-end
  - Express
  - GraphQL Express middlware
  - Mocha
  - Chai

## Test runners
 - front-end: `npm test`
 - back-end: `npm run test-server`

## Code formatting
 - StandardJS

## Side notes

  Since i was chasing double points using GraphQL, there is no direct Redux invocation in this project. Bit i still <3 Redux :)