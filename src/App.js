import React from 'react'
import injectTapEventPlugin from 'react-tap-event-plugin'
import {
  Router,
  Route,
  browserHistory,
  IndexRoute
} from 'react-router'

import { ApolloClient, ApolloProvider, createNetworkInterface } from 'react-apollo'

import Layout from './Layout.js'
import Search from './containers/Search'
import CarOfTheWeek from './containers/CarOfTheWeek'
import ModelDetails from './containers/ModelDetails'
import NotFound from './components/Controls/NotFound'

import { API_HOST } from './constants'

const networkInterface = createNetworkInterface({
  uri: `${API_HOST}/graphql`
})
const client = new ApolloClient({
  networkInterface: networkInterface
})

injectTapEventPlugin()

const App = () =>
  <ApolloProvider client={client}>
    <Router history={browserHistory}>
      <Route path='/' component={Layout}>
        <IndexRoute component={CarOfTheWeek} />
        <Route path='/search' component={Search} />
        <Route path='/make/model/:modelId' component={ModelDetails} />
        <Route path='*' component={NotFound} />
      </Route>
    </Router>
  </ApolloProvider>

export default App
