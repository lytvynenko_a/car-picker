import React from 'react'
import PropTypes from 'prop-types'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import AppBar from 'material-ui/AppBar'

import { NavigationButtons } from './components/Controls'

import './Layout.css'

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#F52400'
  }
})

const Layout = ({ children }) =>
  <MuiThemeProvider muiTheme={muiTheme}>
    <div>
      <AppBar
        title='Qantas Car Picker'
        showMenuIconButton={false}
        iconElementRight={<NavigationButtons />}
      />
      <div className='wrapper'>
        {children}
      </div>
    </div>
  </MuiThemeProvider>

Layout.propTypes = {
  children: PropTypes.any
}

export default Layout
