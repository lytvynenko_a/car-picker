import React from 'react'
/* Stolen from https://codepen.io/RRoberts/pen/pEXWEp */

const Loader = () => <div className='loader' id='loader-6'>
  <span />
  <span />
  <span />
  <span />
</div>

export default Loader
