import React from 'react'
import PropTypes from 'prop-types'
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import FlatButton from 'material-ui/FlatButton'
import Loader from './Loader'
import NotFound from './NotFound'
const styles = {
  imgWrapper: {
    minHeight: 280
  }
}

const CarCard = ({ model, loading, error }) => {
  return loading ? <Loader />
  : error ? <NotFound /> : <Card>
    <CardMedia style={styles.imgWrapper}>
      <img alt={model.name} src={model.imageUrl} />
    </CardMedia>
    <CardTitle title={model.name} />
    { model.review && <CardText>{model.review}</CardText> }
    <CardActions>
      <FlatButton label={`Buy now for $${model.price}`} />
    </CardActions>
  </Card>
}

CarCard.propTypes = {
  model: PropTypes.object,
  loading: PropTypes.bool
}

export default CarCard
