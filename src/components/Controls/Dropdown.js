import React from 'react'
import PropTypes from 'prop-types'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

import './controls.css'

const styles = {
  width: '100%'
}

const DropDown = ({ items, selectedValue, handleChange, defaultText }) =>
  <SelectField
    className='animated slideInDown'
    style={styles}
    value={selectedValue}
    fullWidth
    onChange={handleChange}>
    <MenuItem
      key={`item-default`}
      value={0}
      primaryText={defaultText} />
    {items.map((item) =>
      <MenuItem
        key={`item-${item.id}`}
        value={item.id}
        primaryText={item.name} />
    )}
  </SelectField>

DropDown.propTypes = {
  items: PropTypes.array.isRequired,
  selectedValue: PropTypes.any,
  handleChange: PropTypes.func,
  defaultText: PropTypes.string
}

export default DropDown
