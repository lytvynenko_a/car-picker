import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import RaisedButton from 'material-ui/RaisedButton'

const DetailsLink = ({ modelId }) => {
  if (modelId > 0) {
    return <Link to={`/make/model/${modelId}`}>
      <RaisedButton
        primary
        label='View details' />
    </Link>
  } else {
    return <RaisedButton
      disabled
      label='View details' />
  }
}
DetailsLink.propTypes = {
  modelId: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ])
}

export default DetailsLink
