import React from 'react'
import Loader from '../Loader'
import { shallow } from 'enzyme'

describe('Loader', () => {
  it('should render', () => {
    const wrapper = shallow(<Loader />)
    expect(wrapper.find('span').length).toBe(4)
  })
})
