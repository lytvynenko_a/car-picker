import React from 'react'
import NavigationButtons from '../NavigationButtons'
import { shallow } from 'enzyme'
import { Link } from 'react-router'

describe('Navigation buttons', () => {
  it('should render 2 links', () => {
    const wrapper = shallow(<NavigationButtons />)
    expect(wrapper.find(Link).length).toBe(2)
  })
})
