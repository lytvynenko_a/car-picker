import React from 'react'
import NotFound from '../NotFound'
import { shallow } from 'enzyme'

describe('NotFound', () => {
  it('should render', () => {
    const wrapper = shallow(<NotFound />)
    expect(wrapper.find('img').length).toBe(1)
  })
})
