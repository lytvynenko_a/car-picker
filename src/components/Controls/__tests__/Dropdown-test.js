/* globals jest */
import React from 'react'
import Dropdown from '../Dropdown'
import { shallow } from 'enzyme'

const mockItems = [
  {id: 1, name: 'Name 1'},
  {id: 2, name: 'Name 2'},
  {id: 3, name: 'Name 3'}
]
describe('Dropdown', () => {
  it('should render', () => {
    const wrapper = shallow(
      <Dropdown items={mockItems} onChange={jest.fn()} />
    )
    expect(wrapper).toMatchSnapshot()
  })
})
