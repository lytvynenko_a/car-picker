import React from 'react'
import CarCard from '../CarCard'
import Loader from '../Loader'
import {Card, CardText} from 'material-ui/Card'
import { shallow } from 'enzyme'

const cotwMock = {
  'id': 520,
  'make': 'Mazda',
  'name': 'MX-5',
  'price': 40000,
  'review': 'The Mazda MX-5 is a traditional two-seat sports car, with a lightweight body and rear-wheel drive. It has a folding, fabric roof and is among the least expensive convertibles. This fourth-generation MX-5 is fantastic fun to drive. Motoring magazine Wheels named it Car of the Year for 2016.'
}

const modelMock = {
  'id': 520,
  'make': 'Mazda',
  'name': 'MX-5',
  'price': 40000
}

describe('<CarCard />', () => {
  it('should render Loader', () => {
    const wrapper = shallow(<CarCard loading />)
    expect(wrapper.find(Loader).length).toBe(1)
  })
  it('should render Card for Car or the week payload', () => {
    const wrapper = shallow(<CarCard model={cotwMock} />)
    expect(wrapper.find(Loader).length).toBe(0)
    expect(wrapper.find(Card).length).toBe(1)
    expect(wrapper.find(CardText).length).toBe(1)
  })
  it('should render Card for model payload', () => {
    const wrapper = shallow(<CarCard model={modelMock} />)
    expect(wrapper.find(Loader).length).toBe(0)
    expect(wrapper.find(Card).length).toBe(1)
    expect(wrapper.find(CardText).length).toBe(0)
  })
})
