import React from 'react'
import DetailsLink from '../DetailsLink'
import { shallow } from 'enzyme'
import { Link } from 'react-router'
import RaisedButton from 'material-ui/RaisedButton'

describe('DetailsLink', () => {
  it('should render only button if no modelId prop set', () => {
    const wrapper = shallow(<DetailsLink />)
    expect(wrapper.find(Link).length).toBe(0)
    expect(wrapper.find(RaisedButton).length).toBe(1)
  })
  it('should render a link and button if modelId prop set', () => {
    const wrapper = shallow(<DetailsLink modelId={520} />)
    expect(wrapper.find(Link).length).toBe(1)
    expect(wrapper.find(RaisedButton).length).toBe(1)
  })
})
