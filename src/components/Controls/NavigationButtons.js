import React from 'react'
import { Link } from 'react-router'
import FlatButton from 'material-ui/FlatButton'

import './controls.css'

const NavigationButtons = () =>
  <div className='navigation-buttons'>
    <Link to='/'>
      <FlatButton label='Home' />
    </Link>
    <Link to='/search'>
      <FlatButton label='Search' />
    </Link>
  </div>

export default NavigationButtons
