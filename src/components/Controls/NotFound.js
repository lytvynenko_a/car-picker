import React from 'react'

import './controls.css'

const NotFound = () => <div className='not-found'>
  <img src='/robot.png' />
  <h1> Oops.. not found</h1>
</div>

export default NotFound
