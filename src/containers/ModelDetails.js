import { gql, graphql } from 'react-apollo'

import { CarCard } from '../components/Controls'

export const modelQuery = gql`query ModelQuery($modelId: Int!) {
  model(modelId: $modelId) {
    id
    name,
    make,
    price,
    imageUrl
  }
}`

export const cardWithData = graphql(modelQuery, {
  options: (ownProps) => ({
    variables: {
      modelId: ownProps.params.modelId
    }}),
  props: ({ ownProps, data: { loading, model, error } }) => ({
    loading,
    model,
    error
  })
})

export default cardWithData(CarCard)
