import React, { Component } from 'react'
import { gql, graphql } from 'react-apollo'

import { Dropdown, Loader, DetailsLink } from '../components/Controls'

export const makesQuery = gql`query {
  items: makes {
    id
    name
  }
}`

export const modelsQuery = gql`query ModelsQuery($makeId: Int!) {
  items: models(makeId: $makeId) {
    id
    name
  }
}`

const DropDownElement = ({data: { items, loading }, ...rest }) => loading ? <Loader /> : <Dropdown items={items} {...rest} />

export const MakesDropDownWithData = graphql(makesQuery)(DropDownElement)

export const ModelsDropDownWithData = graphql(modelsQuery, {
  options: ({ makeId }) => ({ variables: { makeId } })
})(DropDownElement)

class Search extends Component {
  state = {
    makeId: 0,
    modelId: 0
  }

  constructor() {
    super()
    this.handleMakeChange = this.handleChange.bind(this, 'makeId')
    this.handleModelChange = this.handleChange.bind(this, 'modelId')
  }

  handleChange(type, evt, idx, value) {
    console.log(type, value)
    this.setState({ [type]: value })
  }

  render () {
    const { makeId, modelId } = this.state
    return <div className="wrapper">
      <div>
        <MakesDropDownWithData 
          selectedValue={makeId} 
          defaultText="Select make" 
          handleChange={this.handleMakeChange} />
      </div>
      <div>
        { (makeId > 0) && <ModelsDropDownWithData 
                          selectedValue={modelId} 
                          defaultText="Select model" 
                          makeId={makeId}
                          handleChange={this.handleModelChange} />
        }
      </div>
      <div className='action-wrapper'>
        <DetailsLink modelId={modelId} />
      </div>
    </div>
  }
}

export default Search
