import { gql, graphql } from 'react-apollo'
import CarCard from '../components/Controls/CarCard'

export const query = gql`query {
carOfTheWeek {
  id
  name
  make
  price
  review
  imageUrl
}
}`

export const cardWithData = graphql(query, {
  props: ({ ownProps, data: { loading, carOfTheWeek, error } }) => ({
    loading,
    model: carOfTheWeek,
    error
  })
})

export default cardWithData(CarCard)
